# Translation of kscreenlocker_greet.po to Euskara/Basque (eu).
# Copyright (C) 2017, The Free Software Foundation.
# This file is distributed under the same license as the kde-workspace package.
# KDE Euskaratzeko proiektuaren arduraduna <xalba@euskalnet.net>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@euskalnet.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kscreenlocker_greet\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-23 00:38+0000\n"
"PO-Revision-Date: 2017-12-02 14:32+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@euskalnet.net>\n"
"Language-Team: Basque <kde-i18n-doc@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: main.cpp:128
#, kde-format
msgid "Greeter for the KDE Plasma Workspaces Screen locker"
msgstr "KDE Plasma Languneen pantaila giltzatzailearen harreragilea"

#: main.cpp:132
#, kde-format
msgid "Starts the greeter in testing mode"
msgstr "Harreragilea proba moduan abiarazten du"

#: main.cpp:135
#, kde-format
msgid "Starts the greeter with the selected theme (only in Testing mode)"
msgstr "Harreragilea hautatutako gaiarekin abiarazten du (Proba moduan soilik)"

#: main.cpp:139
#, kde-format
msgid "Lock immediately, ignoring any grace time etc."
msgstr "Giltzatu berehala, luzapen denborei etab. begiratu gabe"

#: main.cpp:141
#, kde-format
msgid "Delay till the lock user interface gets shown in milliseconds."
msgstr ""
"Atzeratu erabiltzailea giltzatzeko interfazea milisegundotan erakutsi arte."

#: main.cpp:144
#, kde-format
msgid "Don't show any lock user interface."
msgstr "Ez erakutsi erabiltzailea giltzatzeko interfazerik."

#: main.cpp:145
#, kde-format
msgid "Default to the switch user UI."
msgstr "Lehenetsi aldatu erabiltzailea erabiltzaile interfazera."

#: main.cpp:147
#, kde-format
msgid "File descriptor for connecting to ksld."
msgstr "ksld-ra konektatzeko fitxategi deskriptorea."
